# Forex

## Design decisions

1Forge API free tier only allows 1000 requests per day, so very simple
caching solution is implemented.

I used bulk rates request to fetch all supported rates from 1Forge API
and cache them using a simple `MVar` with cache state.

Cache management is segregated within the interpreter implementation
using OOP-ish style. It is not as clear as I would like it to be, but
it's really easy to implement. Another approach would be to create
another service for caching operations, but it's more complicated,
so I decided to go with the first one. Using `MVar` also ensures that
quota won't be exceeded, because it provides synchronization of
concurrent requests.

The downside of the selected approach is that `eff` is kinda useless in
this case. There is only one effect type, `Task` and there is nothing
to compose.

I changed the service interface to expose bulk rates request instead
of single rate, but decided to leave service HTTP API unchanged,
so it only supports rate between 2 currencies. I did so simply because
it's the use case described in the requirement. API can be easily
extended by new method for bulk rates, so I don't think it's critical.

For querying 1Forge API I decided to use `akka-http` because it already
exists in dependencies and does the job decently.

## Minor improvements

Implemented better error handling, at least to some extent.

Extended `ApplicationConfig` to inject state (cache and clock)
into interpreters.

Introduced `enumeratum` to simplify working `Currency` "enum".

Upgraded some dependencies version, for example `monix` (to use
`bracket` and `TaskApp` in `forex.Main` for better resource management)
and `circe` to remove backported unwrapped encoder/decoder.
